# Named Tuple 


```python
from collections import namedtuple

Transition = namedtuple("Transition",("A","B","C"))
transitions = [[1,2,3], [4,2,3], [1,2,2], [5,3,3]]

batch = Transition(*zip(*transitions))
batch.A
batch.B
batch.C
```