from .DQN import DeepQNetwork
from agents import *
from collections import deque
import numpy as np
import torch 
import random 
from .replay_buffer import ReplayBuffer

class DqnAgent():
    def __init__(self,index,  nS, nA, alpha, gamma, eps, eps_min, eps_decay, max_mem=25000):
        self.index = index
        self.nS=nS
        self.nA=nA
        self.alpha=alpha
        self.gamma=gamma
        self.eps=eps
        self.eps_min=eps_min 
        self.eps_decay=eps_decay
        self.model=DeepQNetwork(nS, nA, alpha)
        self.buffer = ReplayBuffer(max_mem)

    def action(self, state):
        if np.random.rand() <= self.eps:
            return random.randrange(self.nA) #Explore
        state = np.expand_dims(state, axis=0)
        action_vals = self.model.predict(state) #Exploit: Use the NN to predict the correct action from this state
        return np.argmax(action_vals[0])

    def test_action(self, state): #Exploit
        action_vals = self.model.predict(state)
        return np.argmax(action_vals[0])

    def store(self, state, action, reward, nstate, done):
        self.buffer.add(state, action, reward, nstate, done)

    def experience_replay(self, batch_size):
        states, actions, rewards, nstates, dones = self.buffer.sample(batch_size)

        state_pred = self.model.predict(states)
        nstate_pred = self.model.predict(nstates)

        target_not_done = rewards + self.gamma * np.amax(nstate_pred, axis=1)
        target_not_done[dones==1] = 0
        target_done = np.array([s[int(actions[i])] for i,s in enumerate(state_pred)])
        target_done[dones==0] = 0


        returns = target_not_done + target_done
        for i in range(batch_size):
            state_pred[i, int(actions[i])] = returns[i]
        
        self.model.fit(states, state_pred)
 
        if self.eps > self.eps_min:
            self.eps *= self.eps_decay