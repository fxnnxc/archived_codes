import torch 
import torch.nn as nn


class DeepQNetwork():
    def __init__(self, states, actions, alpha):
        self.nS = states
        self.nA = actions
        self.alpha = alpha
        self.model = self.build_model()
        self.criterion = nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=1e-4)
                
    def build_model(self):
        model = nn.Sequential(
            nn.Linear(self.nS, 24),
            nn.ReLU(),
            nn.Linear(24, self.nA)

        )
        return model

    def fit(self,x,y):
        h = self.model(torch.tensor(x).float())
        y = torch.tensor(y).float()
        loss = self.criterion(h, y)
        
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    def predict(self, x):
        with torch.no_grad():
            x = torch.tensor(x).float()
            y = self.model(x)
            return y.numpy()
    