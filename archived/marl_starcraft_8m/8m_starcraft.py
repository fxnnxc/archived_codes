from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from smac.env import StarCraft2Env
import numpy as np
import time 
import argparse 




if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--num-episodes", default=5, type=int)
    parser.add_argument("--sleep", default=0.005, type=float)
    config = parser.parse_args()

    env = StarCraft2Env(map_name="8m")
    env_info = env.get_env_info()

    n_actions = env_info["n_actions"]
    n_agents = env_info["n_agents"]


    for i in range(config.num_episodes):        
        env.reset()
        obs = env.get_obs()
        terminated = False
        while not terminated:
            actions = [0 for i in range(n_agents)]
            enemy_count = [0 for i in range(n_actions)]
            for agent_id in range(n_agents):
                avail_actions = env.get_avail_agent_actions(agent_id)
                avail_actions_ind = np.nonzero(avail_actions)[0]
                for av in avail_actions_ind:
                    actions[agent_id] = av
                time.sleep(config.sleep)
                
            reward, terminated, _ = env.step(actions)   