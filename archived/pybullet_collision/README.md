# More information 

[Pybullet Guide](https://docs.google.com/document/d/10sXEhzFRSnvFcl3XxNGhnD4N2SedqwdAvK3dsihxVUA/edit#heading=h.cb0co8y2vuvc)

Search **getContactPoints, getClosestPoints**



```python
getContactPoints(
    bodyA
    bodyB
    linkIndexA
    linkIndexB
    physicsClientId
)
```

```
contactFlag
bodyUniqueIdA
bodyUniqueIdB
linkIndexA
linkIndexB
positionOnA
positionOnB
contactNormalOnB
contactDistance
normalForce
lateralFriction1
lateralFrictionDir1
lateralFriction2
lateralFrictionDir2
```
