import pybullet as p 
import pybullet_data 


class PhysicalMap():
    def __init__(self):
        self.maps = [
                [1,1,1,1,1,1,1],
                [1,0,0,0,0,0,1],
                [1,0,1,1,1,1,1],
                [1,0,0,0,0,0,1],
                [1,1,1,1,1,0,1],
                [1,0,0,0,0,0,1],
                [1,1,1,1,1,1,1],
        ]
        self.r_size = 7
        self.c_size = 7
        self.start_position = [5-self.r_size/2,1-self.c_size/2,0]
        self.objects = {"block" :[]}


    def build(self):
        for ri, row in enumerate(self.maps):
            for ci, element in enumerate(row):
                if self.maps[ri][ci]:
                    self.objects['block'].append(p.loadURDF("cube_small.urdf", [ri-self.r_size/2,ci-self.c_size/2,0], globalScaling=20, useFixedBase=1))

import time 

def main():
    p.connect(p.GUI)
    p.setAdditionalSearchPath(pybullet_data.getDataPath())
    physical_map = PhysicalMap()
    physical_map.build()

    duck = p.loadURDF("duck_vhacd.urdf", physical_map.start_position, globalScaling=5)
    p.resetBaseVelocity(duck, [0,10,0])

    while True :
        p.stepSimulation()
        time.sleep(0.05)
        print("contact info length : ",len(p.getContactPoints(duck)))




main()

