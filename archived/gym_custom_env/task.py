import gym 
from gym import Discrete, Box 
import numpy as np 

class CustomEnv(gym.Env):

    def __init__(self):
        self.observation_space = Box(low=-np.inf, high=np.inf, shape=(42,42))
        self.action_space = Discrete(5)
        self.timestep = 0
        self.max_timestep = 100

    def reset(self):
        return np.random.random(size=(42,42))

    def step(self, action):
        state, reward, done, info = None, 0, False, {}
        state = np.random.random(size=(42,42))
        if self.timestep > self.max_timestep:
            done = True 
            reward = 1
        return state, reward, done, info 


if __name__ == "__main__":
    env = CustomEnv()
    state = env.reset()
    done = False 
    while not done:
        action = np.random.randint(5)
        state, reward, done, info = env.step()
        print(state, reward, don, info)
