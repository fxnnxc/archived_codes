import numpy as np 

import gym
from gym.spaces import Discrete, Box 

import ray 
from ray import tune 
from ray.rllib.models.torch.visionnet import VisionNetwork
from ray.rllib.models import ModelCatalog
from ray.tune.registry import register_env 

class CustomEnv(gym.Env):
    def __init__(self, config={}):
        self.observation_space = Box(low=-np.inf, high=np.inf, shape=(42,42,3))
        self.action_space = Discrete(5)
        self.timestep = 0
        self.max_timestep = 100

    def reset(self):
        return np.random.random(size=(42,42,3))

    def step(self, action):
        state, reward, done, info = None, 0, False, {}
        state = np.random.random(size=(42,42,3))
        if self.timestep > self.max_timestep:
            done = True 
            reward = 1
        return state, reward, done, info 


ModelCatalog.register_custom_model("visionnet", VisionNetwork)


def main():
    ray.init()
    register_env("env", lambda config:CustomEnv(config))

    config = {
        "env" : "env",
        "model":{"custom_model" : "visionnet", "dim": 42, "conv_filters": [[16, [4, 4], 2], [32, [4, 4], 2], [512, [11, 11], 1]]},
        "framework":"torch"
    }

    stop = {'training_iteration':10}
    tune.run(
        "DQN",
        config=config,
        stop = stop,
        checkpoint_at_end=False
    ) 

main()
