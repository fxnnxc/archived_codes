import ray 
from ray import tune 
from ray.rllib.policy.sample_batch import SampleBatch

from ray.rllib.agents.trainer_template import build_trainer
from ray.rllib.policy.torch_policy_template import build_torch_policy

def policy_gradient_loss(policy, model, dist_class, train_batch):
    logits, _ = model.from_batch(train_batch)
    action_dist = dist_class(logits)
    log_probs = action_dist.logp(train_batch[SampleBatch.ACTIONS])
    return -train_batch[SampleBatch.REWARDS].dot(log_probs)


def main():
    ray.init()
    config = {
        "env" : "CartPole-v0",
        "framework":"torch"
    }

    MyTorchPolicy = build_torch_policy(
        name="MyTorchPolicy",
        loss_fn=policy_gradient_loss)

    MyTrainer = build_trainer(
        name="MyCustomTrainer",
        default_policy=MyTorchPolicy)


    stop = {'training_iteration':100}
    tune.run(
        MyTrainer,
        config=config,
        stop = stop,
        checkpoint_at_end=False
    ) 

main()
